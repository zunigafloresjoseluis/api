const Router = require('restify-router').Router,
    router = new Router(),
    parser = require('js2xmlparser2'),
    js2xmlparser = require('js2xmlparser2'),
    Actividad = require('../modelos/Actividad'),
    sequealize = require('../postgres_conect'),
    Entidad = require('../modelos/Entidad'),
    validacion = require('../funciones_validaciones/validar'),
    Correo = require('../modelos/Correos'),
    cb = new validacion();

router.get('/', (req, res, next) => {
    let respuesta = { actividades: [] }

    Actividad.findAll()
        .then(data => {
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {

                    respuesta.actividades.push({
                        clave_actividad: data[i].dataValues.cve_scian,
                        descripcion_actividad: data[i].dataValues.desc_scian,

                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }




            } else {
                res.status(404);
                res.end();
                next();
            }


        })
        .catch(err => {
            console.log(err.stack)
        })
})





router.get('/estados', (req, res, next) => {
    let limite = 100;
    sequealize.query(`select cve_scian,desc_scian,clave_entidad,entidad.nombre from actividad 
        inner join  empresa on (idactividad=cve_scian)
        inner join  ubicacion on(id_empresa=id)
        inner join  entidad  on (clave_entidad=clave)
limit ${limite}`, {
            type: sequealize.QueryTypes.SELECT
        })
        .then(datos => {
            // console.log(datos);
            var estados = [];
            for (let q = 0; q < datos.length; q++) {
                if (!cb.existe_arr(estados, datos[q].clave_entidad)) {
                    estados.push({ id: datos[q].clave_entidad, nombre: datos[q].nombre })
                }

            }
            let respuesta = { estados: [] };
            let obj_entregar = {};
            for (let i = 0; i < estados.length; i++) {

                obj_entregar.id_estado = estados[i].id;
                obj_entregar.nombre_estado = estados[i].nombre;
                obj_entregar.actividades = [];
                for (let j = 0; j < datos.length; j++) {
                    if (estados[i] === datos[j].clave_entidad) {
                        obj_entregar.actividades.push({
                            id_actividad: datos[j].cve_scian,
                            descripcion_actividad: datos[j].desc_scian
                        })
                    }


                }
                respuesta.estados.push(obj_entregar);


            }
            console.log(obj_entregar);
        })
        .catch(err => {
            console.log(err.stack)
        })


})



router.post('/', (req, res, next) => {
    console.log(req.body);

    return sequealize.transaction(trans => {
            return Actividad.create({
                cve_scian: req.body.clave_actividad,
                desc_scian: req.body.descripcion_actividad
            }, { transaction: trans });
        })
        .then(data => {
            res.status(201);
            res.end();
            next();
        })
        .catch(err => {
            console.log(err.stack)
            res.status(400);
            res.end();
            next();
        })

})

router.put('/', (req, res, next) => {
    return sequealize.transaction(trans => {
            return Actividad.update({ desc_scian: req.body.descripcion_actividad }, {
                    where: {
                        cve_scian: req.body.clave_actividad
                    }
                }, {
                    returning: true,
                    plain: true,
                    transaction: trans
                }

            )
        })
        .then(resultado => {
            res.status(201);
            res.end();
            next();
        })
        .catch(err => {
            res.status(400);
            res.end();
            next();
        });

})




module.exports = router;