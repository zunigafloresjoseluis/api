const Router = require('restify-router').Router,
    router = new Router(),
    js2xmlparser = require('js2xmlparser2'),
    Empresa = require('../modelos/Empresa'),
    Ubicacion = require('../modelos/Ubicacion'),
    Entidad = require('../modelos/Entidad'),
    Telefonos = require('../modelos/Telefonos'),
    Correos = require('../modelos/Correos'),
    sequealize = require('../postgres_conect'),
    UbicacionEspecifica = require('../modelos/UbicacionEspecifica'),
    Actividad = require('../modelos/Actividad'),
    Referencias = require('../modelos/Referencias'),
    validacion = require('../funciones_validaciones/validar'),
    cb = new validacion();



router.get('/', (req, res, next) => {
    let respuesta = { empresa: [] };
    Empresa.findAll({

        })
        .then(datos => {
            console.log(datos.length);
            if (datos.length > 0) {
                //console.log(datos);
                for (let i = 0; i < datos.length; i++) {

                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }



            } else {
                res.status(404);
                res.end();
                next();
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        });


});
router.get('/medios_masivos/actividades/:id', (req, res, next) => {
    let respuesta = {}
    Empresa.findOne({
            limit: 100,

            include: [{
                model: Actividad,
                where: {
                    cve_scian: req.params.id
                }
            }]

        })
        .then(datos => {


            respuesta.empresas = {
                id: datos.id,
                nombre: datos.nombre,
                id_actividad: datos.idactividad,
                descripcion_actividad: datos.actividad.desc_scian
            }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);

                    res.end(parser("empresas", respuesta, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;

            }




        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
})
router.get('/medios_masivos/actividades', (req, res, next) => {
    let respuesta = { empresas: [] }
    Empresa.findAll({
            limit: 100,
            where: {
                idactividad: {
                    $ilike: (51 + '%')
                }
            },
            include: [{ model: Actividad }]

        })
        .then(datos => {

            if (datos.length > 0) {
                for (let i = 0; i < datos.length; i++) {

                    respuesta.empresas.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        id_actividad: datos[i].dataValues.idactividad,
                        descripcion_actividad: datos[i].dataValues.actividad.desc_scian
                    })
                }

                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }



            } else {
                res.status(404);
                res.end();

            }


        })
        .catch(data => {})
})
router.get('/seguros_financieras/actividades', (req, res, next) => {
    let respuesta = { empresas: [] }
    Empresa.findAll({
            limit: 100,
            where: {
                idactividad: {
                    $ilike: (52 + '%')
                }
            },
            include: [{ model: Actividad }]

        })
        .then(datos => {

            if (datos.length > 0) {
                for (let i = 0; i < datos.length; i++) {

                    respuesta.empresas.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        id_actividad: datos[i].dataValues.idactividad,
                        descripcion_actividad: datos[i].dataValues.actividad.desc_scian
                    })
                }

                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }



            } else {
                res.status(404);
                res.end();

            }


        })
        .catch(data => {})
})

router.get('/medios_masivos', (req, res, next) => {
    let respuesta = { empresa: [] }
    Empresa.findAll({
            limit: 100,
            where: {
                idactividad: {
                    $ilike: (51 + '%')
                }
            }
        })
        .then(datos => {

            if (datos.length > 0) {
                for (let i = 0; i < datos.length; i++) {

                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre
                    })
                }

                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }

            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })


});


router.get('/seguros_financieras', (req, res, next) => {
    let respuesta = { empresa: [] }
    Empresa.findAll({
            limit: 100,
            where: {
                idactividad: {
                    $ilike: (52 + '%')
                }
            }
        })
        .then(datos => {

            if (datos.length > 0) {

                for (let i = 0; i < datos.length; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }

            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })


});


router.get('/estados', (req, res, next) => {
    let respuesta = { empresa: [] }
    Empresa.findAll({
            offset: 10,
            limit: 100,
            include: [{
                model: Ubicacion,
                include: [{ model: Entidad }]
            }]
        })
        .then(datos => {

            if (datos.length > 0) {

                for (let i = 0; i < datos.length; i++) {

                    respuesta.empresa.push({
                        id_empresa: datos[i].dataValues.id,

                        nombre_empresa: datos[i].dataValues.nombre,
                        clave_estado: datos[i].dataValues.ubicacions[0].dataValues.clave_entidad,
                        nombre: datos[i].dataValues.ubicacions[0].dataValues.entidad.dataValues.nombre
                    })

                }

                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }





            } else {
                res.status(404);
                res.end();
                next()
            }

        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })


})
router.get('/estados/:id', (req, res, next) => {

    let respuesta = { empresa: [] }
    Empresa.findAll({
            offset: 10,
            limit: 100,
            include: [{
                model: Ubicacion,

                where: {
                    clave_entidad: req.params.id
                },
                include: [{
                    model: Entidad
                }]
            }]
        })
        .then(datos => {

            if (datos.length > 0) {

                for (let i = 0; i < datos.length; i++) {

                    respuesta.empresa.push({
                        id_empresa: datos[i].dataValues.id,

                        nombre_empresa: datos[i].dataValues.nombre,
                        clave_estado: datos[i].dataValues.ubicacions[0].dataValues.clave_entidad,
                        nombre: datos[i].dataValues.ubicacions[0].dataValues.entidad.dataValues.nombre
                    })

                }

                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }





            } else {
                res.status(404);
                res.end();
                next()
            }

        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })


})

router.get('/medios_masivos/:id', (req, res, next) => {
    let respuesta = { empresa: [] }
    Empresa.findAll({

            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (51 + '%')
                }
            }
        })
        .then(datos => {
            console.log(datos.length)
            if (datos.length > 0) {
                for (let i = 0; i < datos.length; i++) {

                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre
                    })
                }

                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }

            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })


})

router.get('/seguros_financieras/:id', (req, res, next) => {
    let respuesta = { empresa: [] }
    Empresa.findAll({

            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (52 + '%')
                }

            }
        })
        .then(datos => {

            if (datos.length > 0) {

                for (let i = 0; i < datos.length; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }

            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })



})

router.get('/medios_masivos/:id/contactos', (req, res, next) => {
    let respuesta = { empresa: {} };
    let final_respuesta = { empresa: [] };
    Empresa.findAll({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (51 + '%')
                }
            },
            include: [{ model: Correos }, { model: Telefonos }],

        })
        .then(datos => {
            //console.log(datos[0].dataValues.correos)
            if (datos.length > 0) {
                respuesta.empresa.id_empresa = datos[0].dataValues.id
                respuesta.empresa.nombre = datos[0].dataValues.nombre
                respuesta.empresa.sitio_web = datos[0].dataValues.sitio_web;
                respuesta.empresa.correos = [];
                respuesta.empresa.telefonos = [];
                for (let i = 0; i < datos[0].dataValues.correos.length; i++) {
                    respuesta.empresa.correos.push(datos[0].dataValues.correos[i].dataValues.correo)
                }
                for (let i = 0; i < datos[0].dataValues.telefonos.length; i++) {
                    respuesta.empresa.telefonos.push(datos[0].dataValues.telefonos[i].dataValues.telefono)
                }
                final_respuesta.empresa.push(respuesta);
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresa", final_respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }



            } else {
                res.status(404);
                res.end();
                next()
            }



        })
        .catch(err => {
            console.log(`${err.stack}`)
        })

})

router.get('/seguros_financieras/:id/contactos', (req, res, next) => {
    let respuesta = { empresa: {} };
    Empresa.findAll({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (52 + '%')
                }
            },
            include: [{ model: Correos }, { model: Telefonos }],

        })
        .then(datos => {
            //console.log(datos[0].dataValues.correos)
            if (datos.length > 0) {
                respuesta.empresa.id_empresa = datos[0].dataValues.id
                respuesta.empresa.nombre = datos[0].dataValues.nombre
                respuesta.empresa.sitio_web = datos[0].dataValues.sitio_web;
                respuesta.empresa.correos = [];
                respuesta.empresa.telefonos = [];
                for (let i = 0; i < datos[0].dataValues.correos.length; i++) {
                    respuesta.empresa.correos.push(datos[0].dataValues.correos[i].dataValues.correo)
                }
                for (let i = 0; i < datos[0].dataValues.telefonos.length; i++) {
                    respuesta.empresa.telefonos.push(datos[0].dataValues.telefonos[i].dataValues.telefono)
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresa", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }



            } else {
                res.status(404);
                res.end();
                next()
            }



        })
        .catch(err => {
            console.log(`${err.stack}`)
        })

})

router.get('/medios_masivos/:id/ubicacion_general', (req, res, next) => {
    let respuesta = { empresa: {} };
    Empresa.findOne({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (51 + '%')
                }
            },
            include: [{
                model: Ubicacion,

                include: [{
                    model: Entidad
                }]
            }]

        })
        .then(datos => {
            respuesta.empresa.id_empresa = datos.id;
            respuesta.empresa.nombre_empresa = datos.nombre
            respuesta.empresa.ubicacion_general = []
                //console.log(datos.ubicacions[0].entidad.dataValues.nombre);
            for (let i = 0; i < datos.ubicacions.length; i++) {
                respuesta.empresa.ubicacion_general.push({
                    clave_entidad: datos.ubicacions[i].dataValues.clave_entidad,
                    nombre_etidad: datos.ubicacions[i].entidad.dataValues.nombre,
                    clave_municipio: datos.ubicacions[i].dataValues.clave_municipio,
                    nombre_municipio: datos.ubicacions[i].dataValues.nombre_municipio,
                    clave_localidad: datos.ubicacions[i].dataValues.clave_localidad,
                    nombre_localidad: datos.ubicacions[i].dataValues.nombre_localidad,
                    codigo_postal: datos.ubicacions[i].dataValues.cp,
                    area_geografica_basica: datos.ubicacions[i].dataValues.agbasica
                })
            }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);

                    res.end(parser("empresa", respuesta, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;

            }



        })
        .catch(err => {

        })

})

router.get('/seguros_financieras/:id/ubicacion_general', (req, res, next) => {
    let respuesta = { empresa: {} };
    Empresa.findOne({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (52 + '%')
                }
            },
            include: [{
                model: Ubicacion,

                include: [{
                    model: Entidad
                }]
            }]

        })
        .then(datos => {
            respuesta.empresa.id_empresa = datos.id;
            respuesta.empresa.nombre_empresa = datos.nombre
            respuesta.empresa.ubicacion_general = []
                //console.log(datos.ubicacions[0].entidad.dataValues.nombre);
            for (let i = 0; i < datos.ubicacions.length; i++) {
                respuesta.empresa.ubicacion_general.push({
                    clave_entidad: datos.ubicacions[i].dataValues.clave_entidad,
                    nombre_etidad: datos.ubicacions[i].entidad.dataValues.nombre,
                    clave_municipio: datos.ubicacions[i].dataValues.clave_municipio,
                    nombre_municipio: datos.ubicacions[i].dataValues.nombre_municipio,
                    clave_localidad: datos.ubicacions[i].dataValues.clave_localidad,
                    nombre_localidad: datos.ubicacions[i].dataValues.nombre_localidad,
                    codigo_postal: datos.ubicacions[i].dataValues.cp,
                    area_geografica_basica: datos.ubicacions[i].dataValues.agbasica
                })
            }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);

                    res.end(parser("empresa", respuesta, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;

            }



        })
        .catch(err => {

        })

})

router.get('/medios_masivos/:id/ubicacion_especifica', (req, res, next) => {
    let respuesta = { empresa: {} }
    Empresa.findOne({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (51 + '%')
                }
            },
            include: [{
                model: Ubicacion,
                include: [{ model: UbicacionEspecifica }]
            }]
        })
        .then(datos => {
            //console.log(datos.ubicacions[0].dataValues.ubicacion_especifica)
            respuesta.empresa.id_empresa = datos.id;
            respuesta.empresa.nombre_empresa = datos.nombre;
            respuesta.empresa.ubicaciones_especificas = [];
            for (let i = 0; i < datos.ubicacions.length; i++) {
                respuesta.empresa.ubicaciones_especificas.push({
                    tipo_vialidad: datos.ubicacions[i].dataValues.ubicacion_especifica.tipo_vialidad,
                    nombre_vialidad: datos.ubicacions[i].dataValues.ubicacion_especifica.nombre_vialidad,
                    numero_exterior: datos.ubicacions[i].dataValues.ubicacion_especifica.num_exterior,
                    letra_exterior: datos.ubicacions[i].dataValues.ubicacion_especifica.letra_exterior,
                    nombre_edificio: datos.ubicacions[i].ubicacion_especifica.dataValues.nombre_edifico,
                    piso: datos.ubicacions[i].dataValues.ubicacion_especifica.piso,
                    numero_interior: datos.ubicacions[i].dataValues.ubicacion_especifica.num_interior,
                    letra_interior: datos.ubicacions[i].dataValues.ubicacion_especifica.letra_interior,
                    manzana: datos.ubicacions[i].dataValues.ubicacion_especifica.manzana,
                    tipo_asentamiento: datos.ubicacions[i].dataValues.ubicacion_especifica.tipos_asentamiento,
                    nombre_asentamiento: datos.ubicacions[i].dataValues.ubicacion_especifica.nom_asentamiento,
                    tipo_centro_comercial: datos.ubicacions[i].dataValues.ubicacion_especifica.tipo_centro_com,
                    nombre_centro_comercial: datos.ubicacions[i].dataValues.ubicacion_especifica.nom_centro_com,
                    numero_local: datos.ubicacions[0].dataValues.ubicacion_especifica.nlocal,
                    latitud: datos.ubicacions[i].dataValues.ubicacion_especifica.latitud,
                    longitud: datos.ubicacions[i].dataValues.ubicacion_especifica.longitud
                });

            }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);

                    res.end(parser("empresa", respuesta, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;

            }


        })
        .catch(err => {
            console.log(`${err.stack}`)
        })


})

router.get('/seguros_financieras/:id/ubicacion_especifica', (req, res, next) => {
    let respuesta = { empresa: {} }
    Empresa.findOne({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (52 + '%')
                }
            },
            include: [{
                model: Ubicacion,
                include: [{ model: UbicacionEspecifica }]
            }]
        })
        .then(datos => {
            //console.log(datos.ubicacions[0].dataValues.ubicacion_especifica)
            respuesta.empresa.id_empresa = datos.id;
            respuesta.empresa.nombre_empresa = datos.nombre;
            respuesta.empresa.ubicaciones_especificas = [];
            for (let i = 0; i < datos.ubicacions.length; i++) {
                respuesta.empresa.ubicaciones_especificas.push({
                    tipo_vialidad: datos.ubicacions[i].dataValues.ubicacion_especifica.tipo_vialidad,
                    nombre_vialidad: datos.ubicacions[i].dataValues.ubicacion_especifica.nombre_vialidad,
                    numero_exterior: datos.ubicacions[i].dataValues.ubicacion_especifica.num_exterior,
                    letra_exterior: datos.ubicacions[i].dataValues.ubicacion_especifica.letra_exterior,
                    nombre_edificio: datos.ubicacions[i].ubicacion_especifica.dataValues.nombre_edifico,
                    piso: datos.ubicacions[i].dataValues.ubicacion_especifica.piso,
                    numero_interior: datos.ubicacions[i].dataValues.ubicacion_especifica.num_interior,
                    letra_interior: datos.ubicacions[i].dataValues.ubicacion_especifica.letra_interior,
                    manzana: datos.ubicacions[i].dataValues.ubicacion_especifica.manzana,
                    tipo_asentamiento: datos.ubicacions[i].dataValues.ubicacion_especifica.tipos_asentamiento,
                    nombre_asentamiento: datos.ubicacions[i].dataValues.ubicacion_especifica.nom_asentamiento,
                    tipo_centro_comercial: datos.ubicacions[i].dataValues.ubicacion_especifica.tipo_centro_com,
                    nombre_centro_comercial: datos.ubicacions[i].dataValues.ubicacion_especifica.nom_centro_com,
                    numero_local: datos.ubicacions[0].dataValues.ubicacion_especifica.nlocal,
                    latitud: datos.ubicacions[i].dataValues.ubicacion_especifica.latitud,
                    longitud: datos.ubicacions[i].dataValues.ubicacion_especifica.longitud
                });

            }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);

                    res.end(parser("empresa", respuesta, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;

            }


        })
        .catch(err => {
            console.log(`${err.stack}`)
        })


})

router.get('/medios_masivos/:id/ubicacion_especifica/referencias', (req, res, next) => {
    let respuesta = { empresa: {} };
    sequealize.query(` select empresa.id,empresa.nombre,ubicacion.id_ubicacion,referencias.tipo_vialidad,referencias.nombre_vialidad from referencias 
    inner join  ubicacion_especifica on (referencias.id_especifica=ubicacion_especifica.id_especifica)
    inner join ubicacion on(ubicacion.id_ubicacion=ubicacion_especifica.id_ubicacion)
    inner join empresa on(empresa.id=ubicacion.id_empresa)
    where empresa.id=:id and empresa.idactividad like '%51'   limit 3 
    `, {
            replacements: {
                id: req.params.id
            },
            type: sequealize.QueryTypes.SELECT
        })
        .then(datos => {
            if (datos.length > 0) {
                respuesta.empresa.id_empresa = datos[0].id;
                respuesta.empresa.nombre_empresa = datos[0].nombre;
                respuesta.ubicacion_referencia = [];
                var ubicacion = [];
                for (let q = 0; q < datos.length; q++) {
                    if (!cb.existe_arr(ubicacion, datos[q].id_ubicacion)) {
                        ubicacion.push(datos[q].id_ubicacion)
                    }

                }

                for (let i = 0; i < ubicacion.length; i++) {
                    let obj_ubicacion = {};
                    let arre_ubicacion = [];
                    for (let j = 0; j < datos.length; j++) {
                        if (ubicacion[i] === datos[j].id_ubicacion) {
                            obj_ubicacion.tipo_vialidad = datos[j].tipo_vialidad
                            obj_ubicacion.nombre_vialidad = datos[j].nombre_vialidad
                            arre_ubicacion.push(obj_ubicacion);

                        }

                    }
                    respuesta.ubicacion_referencia.push(arre_ubicacion);

                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresa", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }



            }



        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
})


router.get('/seguros_financieras/:id/ubicacion_especifica/referencias', (req, res, next) => {
    let respuesta = { empresa: {} };
    sequealize.query(` select empresa.id,empresa.nombre,ubicacion.id_ubicacion,referencias.tipo_vialidad,referencias.nombre_vialidad from referencias 
    inner join  ubicacion_especifica on (referencias.id_especifica=ubicacion_especifica.id_especifica)
    inner join ubicacion on(ubicacion.id_ubicacion=ubicacion_especifica.id_ubicacion)
    inner join empresa on(empresa.id=ubicacion.id_empresa)
    where empresa.id=:id and empresa.idactividad like '%52'   limit 3 
    `, {
            replacements: {
                id: req.params.id
            },
            type: sequealize.QueryTypes.SELECT
        })
        .then(datos => {
            if (datos.length > 0) {
                respuesta.empresa.id_empresa = datos[0].id;
                respuesta.empresa.nombre_empresa = datos[0].nombre;
                respuesta.ubicacion_referencia = [];
                var ubicacion = [];
                for (let q = 0; q < datos.length; q++) {
                    if (!cb.existe_arr(ubicacion, datos[q].id_ubicacion)) {
                        ubicacion.push(datos[q].id_ubicacion)
                    }

                }

                for (let i = 0; i < ubicacion.length; i++) {
                    let obj_ubicacion = {};
                    let arre_ubicacion = [];
                    for (let j = 0; j < datos.length; j++) {
                        if (ubicacion[i] === datos[j].id_ubicacion) {
                            obj_ubicacion.tipo_vialidad = datos[j].tipo_vialidad
                            obj_ubicacion.nombre_vialidad = datos[j].nombre_vialidad
                            arre_ubicacion.push(obj_ubicacion);

                        }

                    }
                    respuesta.ubicacion_referencia.push(arre_ubicacion);

                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresa", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }



            }



        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
})


/*********************************POST*************************** */
router.post('/', (req, res, next) => {
    return sequealize.transaction(trans => {
            console.log(req.body)
            return Empresa.create({
                id: req.body.id_empresa,
                nombre: req.body.nombre_empresa,
                razon_social: req.body.razon_social,
                personal_ocupado: req.body.personal_ocupado,
                sitio_web: req.body.sitio_web,
                tipo_establecimiento: req.body.tipo_establecimiento,
                fecha_incorporacion: req.body.fecha_incorporacion,
                idactividad: req.body.id_actividad
            }, {
                transaction: trans
            })
        })
        .then(data => {
            res.status(201);
            res.end();
            next();
        })
        .catch(err => {
            console.log(err.stack)
            res.status(400);
            res.end();
            next();
        })

})

router.put('/:id', (req, res, next) => {
    return sequealize.transaction(trans => {
            return Empresa.update({
                nombre: req.body.nombre_empresa,
                razon_social: req.body.razon_social,
                personal_ocupado: req.body.personal_ocupado,
                sitio_web: req.body.sitio_web,
                tipo_establecimiento: req.body.tipo_establecimiento,
                fecha_incorporacion: req.body.fecha_incorporacion,
                idactividad: req.body.id_actividad
            }, { where: { id: req.params.id } }, {
                returning: true,
                plain: true,
                transaction: trans
            })
        })
        .then(resultado => {
            res.status(201);
            res.end();
            next();
        })
        .catch(err => {
            res.status(400);
            res.end();
            next();
        });
})

router.post('/:id/telefono', (req, res, next) => {
    if (req.body.telefono != undefined) {
        return sequealize.transaction(trans => {
                return Telefonos.create({
                    id_empresa: req.params.id,
                    telefono: req.body.telefono
                })
            })
            .then(data => {
                res.status(201);
                res.end();
                next();
            })
            .catch(err => {
                console.log(err.stack)
                res.status(400);
                res.end();
                next();
            })

    } else {
        res.status(400);
        res.end();
        next();
    }



})


router.put('/:id/telefono', (req, res, next) => {
    if (req.body.telfono_nuevo != undefined && req.params.telefono_actual != undefined) {
        return sequealize.transaction(trans => {
                return Telefonos.update({
                    telefono: req.body.telfono_nuevo
                }, {
                    where: {
                        id_empresa: req.params.id,
                        telefono: req.body.telefono_actual
                    }
                }, {
                    returning: true,
                    plain: true,
                    transaction: trans
                })
            })
            .then(data => {
                res.status(201);
                res.end();
                next();
            })
            .catch(err => {
                console.log(err.stack)
                res.status(400);
                res.end();
                next();
            })

    } else {
        res.status(400);
        res.end();
        next();
    }




})

router.post('/:id/correo', (req, res, next) => {
    if (req.body.correo != undefined) {
        return sequealize.transaction(trans => {
                return Correos.create({
                    id_empresa: req.params.id,
                    correo: req.body.correo
                })
            })
            .then(data => {
                res.status(201);
                res.end();
                next();
            })
            .catch(err => {
                console.log(err.stack)
                res.status(400);
                res.end();
                next();
            })
    } else {
        res.status(400);
        res.end();
        next();
    }





})


router.put('/:id/correo', (req, res, next) => {
    if (req.body.correo_nuevo != undefined && req.body.correo_actual != undefined) {
        return sequealize.transaction(trans => {
                return Correos.update({
                    correo: req.body.correo_nuevo
                }, {
                    where: {
                        id_empresa: req.params.id,
                        correo: req.body.correo_actual
                    }
                }, {
                    returning: true,
                    plain: true,
                    transaction: trans
                })
            })
            .then(data => {
                res.status(201);
                res.end();
                next();
            })
            .catch(err => {
                console.log(err.stack)
                res.status(400);
                res.end();
                next();
            })


    } else {
        res.status(400);
        res.end();
        next();
    }




})



/*
		


*/

module.exports = router;