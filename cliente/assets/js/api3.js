var obtener=(function(){
    
    var host="localhost";
    var puerto="8080";

    var _hora=function(){
        var pa=document.getElementById("hora");
        pa.textContent+=metodo.getHora();
    }

    function initMap(lat,long) {
        var punto = {lat: lat, lng: long};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: punto
        });
        var marker = new google.maps.Marker({
          position: punto,
          map: map
        });
      }

        function activateModal() {
            // initialize modal element
            var modalEl = document.createElement('div');
            modalEl.style.width = '400px';
            modalEl.style.height = '300px';
            modalEl.style.margin = '100px auto';
            modalEl.style.backgroundColor = '#fff';
        
            // show modal
            mui.overlay('on', modalEl);
        }
    
        var _empresaE=function(){
            _hora();
            var id=_getUrl("id");
            metodo.get({url:"http://"+host+":"+puerto+"/empresas/seguros-financieras/"+id,
            en_caso_de_exito:function(re){
                var contenedor=document.getElementById("especifica2");
                var objeto=JSON.parse(re.responseText);
    
                if(contenedor.children.length>0){
                        contenedor.innerHTML='';
                }
                
                    var div1=document.createElement("div");
                    div1.setAttribute("class","col-xs-6 col-md-9");
                    var articulo=document.createElement("article");
    
                    var parrafo=document.createElement("p");
                        parrafo.textContent="ID: "+objeto.empresa[0].id;
                        articulo.appendChild(parrafo);
                    
                    parrafo=document.createElement("p");
                        parrafo.textContent="Nombre de la empresa: "+objeto.empresa[0].nombre;
                        articulo.appendChild(parrafo);

                        parrafo=document.createElement("p");
                        parrafo.textContent="Razón social: "+objeto.empresa[0].razon_social;
                        articulo.appendChild(parrafo);
                    
                        
                        _getContactos(id);
                        _getUbicacionE(id);
                        _getReferencias(id);
                        _getUbicacionG(id);
    
                        div1.appendChild(articulo);
                    contenedor.appendChild(div1);
               
            },en_caso_de_error:function(re){
                console.log("error");
            }})
        }
    
        var _getContactos=function(id){
            metodo.get({url:"http://"+host+":"+puerto+"/empresas/seguros-financieras/"+id+"/contactos",
            en_caso_de_exito:function(re){
                var c=JSON.parse(re.responseText);
                var cont=document.getElementById("contactosM");
    
                var p=document.createElement("p");
                p.setAttribute("class","negrita");
                p.textContent="Sitio web";
                cont.appendChild(p);
                
                p=document.createElement("p");
                p.textContent=c.empresa.sitio_web;
                cont.appendChild(p);
    
                var p=document.createElement("p");
                p.setAttribute("class","negrita");
                p.textContent="Correo";
                cont.appendChild(p);
                
                p=document.createElement("p");
                var lista=document.createElement("ul");
                for(var i=0;i<c.empresa.correos.length;i++){
                    var op=document.createElement("li");
                    op.textContent=c.empresa.correos[i];
                    lista.appendChild(op);
                }
                p.appendChild(lista);
                cont.appendChild(p);
    
                var p=document.createElement("p");
                p.setAttribute("class","negrita");
                p.textContent="Telefono";
                cont.appendChild(p);
                
                p=document.createElement("p");
                var lista=document.createElement("ul");
                for(var i=0;i<c.empresa.correos.length;i++){
                    var op=document.createElement("li");
                    op.textContent=c.empresa.telefonos[i];
                    lista.appendChild(op);
                }
                p.appendChild(lista);
                cont.appendChild(p);
                
            },
            en_caso_de_error:function(re){
                console.log("error");
            }
        })};
    
        var _getUbicacionG=function(id){
            metodo.get({url:"http://"+host+":"+puerto+"/empresas/seguros-financieras/"+id+"/ubicacion-general",
            en_caso_de_exito:function(re){
                var ube=JSON.parse(re.responseText);
                var cont=document.getElementById("ubicacion_g");
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Clave de la entidad: "+(ube.empresa.ubicacion_general[0].clave_entidad);
                    cont.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Nombre de la entidad: "+(ube.empresa.ubicacion_general[0].nombre_entidad);
                    cont.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Clave del municipio: "+(ube.empresa.ubicacion_general[0].clave_municipio);
                    cont.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Nombre del municipio: "+(ube.empresa.ubicacion_general[0].nombre_municipio);
                    cont.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Clave de la localidad: "+(ube.empresa.ubicacion_general[0].clave_localidad);
                    cont.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Nombre de la localidad: "+(ube.empresa.ubicacion_general[0].nombre_localidad);
                    cont.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Código postal: "+(ube.empresa.ubicacion_general[0].codigo_postal);
                    cont.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Área Geográfica Básica: "+(ube.empresa.ubicacion_general[0].area_geografica_basica);
                    cont.appendChild(parrafo);
            },
            en_caso_de_error:function(){
                console.log("error");
            }
            })
    }
    
        var _getUbicacionE=function(id){
            metodo.get({url:"http://"+host+":"+puerto+"/empresas/seguros-financieras/"+id+"/ubicacion-especifica",
            en_caso_de_exito:function(re){
                var ube1=JSON.parse(re.responseText);
                var cont1=document.getElementById("ubicacion_e");
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Tipo de vialidad: "+(ube1.empresa.ubicaciones_especificas[0].tipo_vialidad);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Nombre de la vialidad: "+(ube1.empresa.ubicaciones_especificas[0].nombre_vialidad);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Número exterior: "+(ube1.empresa.ubicaciones_especificas[0].numero_exterior);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Letra exterior: "+(ube1.empresa.ubicaciones_especificas[0].letra_exterior);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Piso: "+(ube1.empresa.ubicaciones_especificas[0].piso);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Número interior: "+(ube1.empresa.ubicaciones_especificas[0].numero_interior);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Letra interior: "+(ube1.empresa.ubicaciones_especificas[0].letra_interior);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Manzana: "+(ube1.empresa.ubicaciones_especificas[0].manzana);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Tipo de asentamiento: "+(ube1.empresa.ubicaciones_especificas[0].tipo_asentamiento);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Nombre del asentamiento: "+(ube1.empresa.ubicaciones_especificas[0].nombre_asentamiento);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Tipo de centro comercial: "+(ube1.empresa.ubicaciones_especificas[0].tipo_centro_comercial);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Nombre del centro comercial: "+(ube1.empresa.ubicaciones_especificas[0].nombre_centro_comercial);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Número de local: "+(ube1.empresa.ubicaciones_especificas[0].numero_local);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Latitud: "+(ube1.empresa.ubicaciones_especificas[0].latitud);
                    cont1.appendChild(parrafo);
    
                    parrafo=document.createElement("p");
                    parrafo.textContent="Longitud: "+(ube1.empresa.ubicaciones_especificas[0].longitud);
                    cont1.appendChild(parrafo);

                    initMap(Number.parseFloat(ube1.empresa.ubicaciones_especificas[0].latitud),Number.parseFloat(ube1.empresa.ubicaciones_especificas[0].longitud));
                    
                    
            },
            en_caso_de_error:function(){
                console.log("error");
            }
            })
        }
    
        var _getReferencias=function(id){
            metodo.get({url:"http://"+host+":"+puerto+"/empresas/seguros-financieras/"+id+"/ubicacion-especifica/referencias",
            en_caso_de_exito:function(re){
                var c=JSON.parse(re.responseText);
                var cont=document.getElementById("referencias");
    
                for(var i=0;i<c.empresa.ubicaciones_referencia.ubicacion.length;i++){
    
                    p=document.createElement("p");
                    p.setAttribute("class","negrita");
                    p.textContent="Referencia "+(i+1);
                    cont.appendChild(p);
    
                    var p=document.createElement("p");
                    p.textContent="Tipo de vialidad:";
                    cont.appendChild(p);
    
                    p=document.createElement("p");
                    p.textContent=c.empresa.ubicaciones_referencia.ubicacion[i].tipo_vialidad
                    cont.appendChild(p);
    
                    p=document.createElement("p");
                    p.textContent="Nombre de la vialidad:";
                    cont.appendChild(p);
    
                    p=document.createElement("p");
                    p.textContent=c.empresa.ubicaciones_referencia.ubicacion[i].nombre_vialidad;
                    cont.appendChild(p);
                }
                
            },
            en_caso_de_error:function(re){
                console.log("error");
            }
        })
        }
    
        var _getUrl=function(name, url) {
            if (!url) url = window.location.search;
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),results =regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    
        return{
            empresaE:_empresaE
        }
    })();