var metodo=(function(){

    var _getHora=function(){
        var d = new Date();

        return ""+d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear()+" a las "+d.getHours()+":"+d.getMinutes()+" hrs";
    }

    var _get=function(obj_parametros){
        var url=obj_parametros.url;
        var xhr=new XMLHttpRequest();
        
        xhr.onreadystatechange=_atender(xhr,obj_parametros);
        xhr.open("GET",url,true);
        xhr.setRequestHeader("Accept",'application/json');
        xhr.send();
    };

    var _atender=function(obj_xhr,obj_parametros){
        return function(){
            
            if(obj_xhr.readyState===4){
            if(obj_xhr.status>=200&&obj_xhr.status<300){
                obj_parametros.en_caso_de_exito(obj_xhr);
            }else{
                obj_parametros.en_caso_de_error(obj_xhr);
            }
        }
        }
    }
    return{
        get:_get,
        atender:_atender,
        getHora:_getHora
    }
})();
