var api=(function(){
    var host="localhost";
    var puerto="8080";

    var _hora=function(){
        var pa=document.getElementById("hora");
        pa.textContent+=metodo.getHora();
    }

    var _listarActividades2=function(){
        _hora();
        var b1=document.getElementById("mostrar2");
        
        b1.addEventListener("click",function(){
            metodo.get({url:"http://"+host+":"+puerto+"/empresas/seguros-financieras",en_caso_de_exito:function(re){
                var contenedor=document.getElementById("eSeguros");
                var objeto=JSON.parse(re.responseText);

                if(contenedor.children.length>0){
                        contenedor.innerHTML='';
                }
                
                for(var i=0;i<objeto.empresa.length;i++){
                   
                    var div1=document.createElement("div");
                    div1.setAttribute("class","col-xs-12 col-sm-6");
                    var articulo=document.createElement("article");
                    articulo.setAttribute("class","borde");

                    var parrafo=document.createElement("p");
                        parrafo.textContent="ID: "+objeto.empresa[i].id;
                        articulo.appendChild(parrafo);
                    
                    parrafo=document.createElement("p");
                        parrafo.textContent="Nombre: "+objeto.empresa[i].nombre;
                        articulo.appendChild(parrafo);

                        parrafo=document.createElement("p");
                        parrafo.textContent="Razón social: "+objeto.empresa[i].razon_social;
                        articulo.appendChild(parrafo);
                    
                        var liga=document.createElement("a");
                        liga.setAttribute("href","../../empresas/id_seguros.html?id="+objeto.empresa[i].id);
                        
                        var mas=document.createElement("button");
                        mas.setAttribute("class","masI btn btn-primary");
                        mas.setAttribute("id",objeto.empresa[i].id);
                        mas.textContent="Más información";
                        

                        liga.appendChild(mas);
                        articulo.appendChild(liga);
                        div1.appendChild(articulo);
                    contenedor.appendChild(div1);
                   
                }
               
            },en_caso_de_error:function(re){
                console.log("error");
            }})
        },false);
    }

    return{
        listarActividades2:_listarActividades2
    }
})();