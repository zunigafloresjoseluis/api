var restify = require('restify');
var server = restify.createServer(),
    Router = require('restify-router').Router,
    router = new Router(),
    empresa = require('./routes/empresa'),
    actividad = require('./routes/actividades'),
    foraneas = require('./foraneas')
server.use(restify.plugins.bodyParser({ mapParams: false }));
server.use(restify.plugins.queryParser({ mapParams: false }));

router.add('/empresas', empresa);
//router.add('/actividades', actividad);

router.applyRoutes(server);



module.exports = server;