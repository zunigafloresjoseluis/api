var Sequelize = require('sequelize'),
    connection = require('../postgres_conect');

const Empresa = connection.define('empresa', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        field: 'id'
    },
    nombre: {
        type: Sequelize.INTEGER,
        field: 'nombre',
    },
    razon_social: {
        type: Sequelize.STRING,
        field: 'razon_social',
        validate: {
            is: ["[a-z]+(\s|[a-z])*$", 'i']
        }
    },
    personal_ocupado: {
        type: Sequelize.STRING,
        field: 'personal_ocupado',
    },
    sitio_web: {
        type: Sequelize.STRING,
        field: 'sitio_web'

    },
    tipo_establecimiento: {
        type: Sequelize.STRING,
        fiedl: 'tipo_establecimiento'
    },
    fecha_incorporacion: {
        type: Sequelize.STRING,
        field: 'fecha_incorporacion'
    },
    idactividad: {
        type: Sequelize.STRING,
        field: 'idactividad'
    }



}, {
    timestamps: false,
    freezeTableName: false,
    tableName: 'empresa'
})

module.exports = Empresa;