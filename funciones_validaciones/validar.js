const xmlparser = require('js2xmlparser');

class validacion {
    cabeceras(arr) {
        var campos = arr.split(', ');
        var temporal;
        for (let i = 0; i < campos.length; i++) {
            if (campos[i] === '*/*') {
                temporal = '*/*';
                break;
            }
            if (campos[i] === 'application/xml') {
                temporal = 'application/xml';
                break;
            }
            if (campos[i] === 'application/json') {
                temporal = 'application/json';
                break;
            }
            if ((i + 1) === campos.length) {
                temporal = 'invalid';
                break;
            }
        };
        return temporal;


    }
    valida_nombre(nombre) {
        if (nombre) {
            return nombre
        } else {
            if (nombre == '')
                return null;
            else
                return '%%';
        }



    }

    existe_arr(arreglo, valor) {
        for (let i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == valor) {
                return true;
            }
        }
        return false;
    }




}
module.exports = validacion;