var Sequelize = require('sequelize'),
    connection = require('../postgres_conect');

const Referencias = connection.define('referencias', {
    id_especifica: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        field: 'id_especifica',

        allowNull: false
    },
    tipo_vialidad: {
        type: Sequelize.STRING,
        field: 'tipo_vialidad'
    },
    nombre_vialidad: {
        type: Sequelize.STRING,
        field: 'nombre_vialidad'

    }

}, {
    timestamps: false,
    freezeTableName: false,
    tableName: 'referencias'
})

module.exports = Referencias;