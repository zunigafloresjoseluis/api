var Sequelize = require('sequelize'),
    connection = require('../postgres_conect');

const Entidad = connection.define('entidad', {
    clave: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
        field: 'clave',
        allowNull: false
    },
    nombre: {
        type: Sequelize.STRING,
        field: 'nombre'
    }
}, {
    timestamps: false,
    freezeTableName: false,
    tableName: 'entidad'
})

module.exports = Entidad