var Sequelize = require('sequelize'),
    connection = require('../postgres_conect');

const UbicacionEspecifica = connection.define('ubicacion_especifica', {
    id_ubicacion: {
        type: Sequelize.NUMERIC,
        field: 'id_ubicacion',
        validate: {
            not: ["[a-z]", 'i'],

        }
    },

    id_especifica: {
        type: Sequelize.INTEGER,
        field: "id_especifica",
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
        validate: {
            not: ["[a-z]", 'i'],

        }
    },

    tipo_vialidad: {
        type: Sequelize.STRING,
        field: 'tipo_vialidad'

    },
    nombre_vialidad: {
        type: Sequelize.STRING,
        field: 'nombre_vialidad'
    },
    num_exterior: {
        type: Sequelize.STRING,
        field: 'num_exterior'
    },
    letra_exterior: {
        type: Sequelize.STRING,
        field: 'letra_exterior'
    },
    nombre_edificio: {
        type: Sequelize.STRING,
        field: 'nombre_edificio'
    },
    piso: {
        type: Sequelize.STRING,
        field: 'piso'
    },
    num_interior: {
        type: Sequelize.STRING,
        field: 'num_interior'
    },
    letra_interior: {
        type: Sequelize.STRING,
        field: 'letra_interior'
    },
    manzana: {
        type: Sequelize.STRING,
        field: 'manzana'
    },
    tipos_asentamiento: {
        type: Sequelize.STRING,
        field: 'tipos_asentamiento'
    },
    nom_asentamiento: {
        type: Sequelize.STRING,
        field: 'nom_asentamiento'
    },
    tipo_centro_com: {
        type: Sequelize.STRING,
        field: 'tipo_centro_com'


    },
    nom_centro_com: {
        type: Sequelize.STRING,
        field: 'nom_centro_com'
    },
    nlocal: {
        type: Sequelize.STRING,
        field: 'nlocal'

    },
    latitud: {
        type: Sequelize.STRING,
        field: 'latitud'

    },
    longitud: {
        type: Sequelize.STRING,
        field: 'longitud'

    }
}, {
    timestamps: false,
    freezeTableName: false,
    tableName: 'ubicacion_especifica'
})
module.exports = UbicacionEspecifica;