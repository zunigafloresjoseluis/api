var Sequelize = require('sequelize'),
    connection = require('../postgres_conect');

const Correo = connection.define('correos', {
    id_empresa: {
        type: Sequelize.INTEGER,
        field: 'id_empresa',
        primaryKey: true,
        validate: {
            not: ["[a-z]", 'i']
        }
    },
    correo: {
        type: Sequelize.STRING,
        field: "correo",
        validate: {
            isEmail: true
        }
    }


}, {
    timestamps: false,
    freezeTableName: false,
    tableName: 'correos'
})

module.exports = Correo;