const Router = require('restify-router').Router,
    router = new Router(),
    parser = require('js2xmlparser2'),
    Empresa = require('../modelos/Empresa'),
    Ubicacion = require('../modelos/Ubicacion'),
    Entidad = require('../modelos/Entidad'),
    Telefonos = require('../modelos/Telefonos'),
    Correos = require('../modelos/Correos'),
    sequealize = require('../postgres_conect'),
    UbicacionEspecifica = require('../modelos/UbicacionEspecifica'),
    Actividad = require('../modelos/Actividad'),
    Referencias = require('../modelos/Referencias'),
    validacion = require('../funciones_validaciones/validar'),
    cb = new validacion();


router.get('/', (req, res, next) => {
    let respuesta = { empresa: [] };
    let limites=cb.limites(req.query.limite_inferior,req.query.limite_superior);
    let orden=cb.orden(req.query.tipo_orden);
    let busqueda='%';
    if(req.query.nombre_empresa!=undefined){
        busqueda='%'+req.query.nombre_empresa+'%';
    }
    if(limites[0]===-1||orden==="error"){
        res.status(404);
        res.end();
        next();
    }else{
    Empresa.findAll({
        order:[orden],
        where: {
                nombre: {$ilike: busqueda}
            }
        })
        .then(datos => {
            console.log(datos.length);
            if (datos.length > 0) {
                if(limites[1]>datos.length){
                    limites[1]=datos.length;
                }
                for (let i = limites[0]; i < limites[1]; i++) {
                    //VALIDACION VACIOS
                    if(datos[i].dataValues.razon_social==="\"\""){
                            datos[i].dataValues.razon_social="N/D";
                        }
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next();
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        });
    }
});

//RUTAS DE MEDIOS MASIVOS
router.get('/medios-masivos', (req, res, next) => {
    let respuesta = { empresa: [] }
    let limites=cb.limites(req.query.limite_inferior,req.query.limite_superior);
    let orden=cb.orden(req.query.tipo_orden);
    let busqueda='%';
    if(req.query.nombre_empresa!=undefined){
        busqueda='%'+req.query.nombre_empresa+'%';
    }
    if(limites[0]===-1||orden==="error"){
        res.status(404);
        res.end();
        next();
    }else{
    Empresa.findAll({
         order:[orden],
            where: {
            idactividad: {$ilike: (51 + '%')},
            nombre: {$ilike: busqueda}
            }
        })
        .then(datos => {
            if (datos.length > 0) {
                if(limites[1]>datos.length){
                    limites[1]=datos.length;
                }
                for (let i = limites[0]; i < limites[1]; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                       res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })
    }
});

router.get('/medios-masivos/actividades', (req, res, next) => {
    let respuesta = { empresa: [] }
    let limites=cb.limites(req.query.limite_inferior,req.query.limite_superior);
    let orden=cb.orden(req.query.tipo_orden);
    let busqueda='%';
    if(req.query.nombre_actividad!=undefined){
        busqueda='%'+req.query.nombre_actividad+'%';
    }
    if(limites[0]===-1||orden==="error"){
        res.status(404);
        res.end();
        next();
    }else{
    Empresa.findAll({
            order:[orden],
            where: {
                idactividad: {
                    $ilike: (51 + '%')
                }
            },
            include: [{ model: Actividad ,where: {desc_scian :{$ilike: busqueda}}}]
        })
        .then(datos => {

            if (datos.length > 0) {
                if(limites[1]>datos.length){
                    limites[1]=datos.length;
                }
                for (let i = limites[0]; i < limites[1]; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social,
                        id_actividad: datos[i].dataValues.idactividad,
                        descripcion_actividad: datos[i].dataValues.actividad.desc_scian
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
            }
        })
        .catch(data => {})
    }
});

router.get('/medios-masivos/:id', (req, res, next) => {
    let respuesta = { empresa: [] }
    Empresa.findAll({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (51 + '%')
                }
            },
            include: [{ model: Actividad }]
        })
        .then(datos => {
            console.log(datos)
            if (datos.length > 0) {
                for (let i = 0; i < datos.length; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social,
                        personal_ocupado: datos[i].dataValues.personal_ocupado,
                        clave_actividad: datos[i].dataValues.idactividad,
                        descripcion_actividad: datos[i].dataValues.actividad.desc_scian
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("empresas", respuesta , { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })
});

router.get('/medios-masivos/:id/contactos', (req, res, next) => {
    let respuesta = { empresa: {} };
    if(Number.isInteger(Number(req.params.id))){
    let final_respuesta = { empresa: [] };
    Empresa.findAll({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (51 + '%')
                }
            },
            include: [{ model: Correos }, { model: Telefonos }],
        })
        .then(datos => {
            //console.log(datos[0].dataValues.correos)
            if (datos.length > 0) {
                respuesta.empresa.id_empresa = datos[0].dataValues.id
                respuesta.empresa.nombre = datos[0].dataValues.nombre
                respuesta.empresa.razon_social=datos[0].dataValues.razon_social;
                respuesta.empresa.sitio_web = datos[0].dataValues.sitio_web;
                respuesta.empresa.correos = [];
                respuesta.empresa.telefonos = [];
                for (let i = 0; i < datos[0].dataValues.correos.length; i++) {
                    respuesta.empresa.correos.push(datos[0].dataValues.correos[i].dataValues.correo)
                }
                for (let i = 0; i < datos[0].dataValues.telefonos.length; i++) {
                    respuesta.empresa.telefonos.push(datos[0].dataValues.telefonos[i].dataValues.telefono)
                }
               // final_respuesta.empresa.push(respuesta);
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("empresa", respuesta.empresa, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
    } else {
        res.status(404);
        res.end();
        next()
    }
});

router.get('/medios-masivos/:id/ubicacion-general', (req, res, next) => {
    let respuesta = { empresa: {} };
    if(Number.isInteger(Number(req.params.id))){
    Empresa.findOne({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (51 + '%')
                }
            },
            include: [{
                model: Ubicacion,

                include: [{
                    model: Entidad
                }]
            }]
        })
        .then(datos => {
            if(datos!=null){
            respuesta.empresa.id_empresa = datos.id;
            respuesta.empresa.nombre_empresa = datos.nombre
            respuesta.empresa.razon_social=datos.razon_social
            respuesta.empresa.ubicacion_general = []
                //console.log(datos.ubicacions[0].entidad.dataValues.nombre);
            for (let i = 0; i < datos.ubicacions.length; i++) {
                respuesta.empresa.ubicacion_general.push({
                    clave_entidad: datos.ubicacions[i].dataValues.clave_entidad,
                    nombre_entidad: datos.ubicacions[i].entidad.dataValues.nombre,
                    clave_municipio: datos.ubicacions[i].dataValues.clave_municipio,
                    nombre_municipio: datos.ubicacions[i].dataValues.nombre_municipio,
                    clave_localidad: datos.ubicacions[i].dataValues.clave_localidad,
                    nombre_localidad: datos.ubicacions[i].dataValues.nombre_localidad,
                    codigo_postal: datos.ubicacions[i].dataValues.cp,
                    area_geografica_basica: datos.ubicacions[i].dataValues.agbasica
                })
            }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);
                    res.end(parser("empresa", respuesta.empresa, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;
            }
            }else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
        })
    } else {
        res.status(404);
        res.end();
        next()
    }
});

router.get('/medios-masivos/:id/ubicacion-especifica', (req, res, next) => {
    let respuesta = { empresa: {} }
    if(Number.isInteger(Number(req.params.id))){
    Empresa.findOne({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (51 + '%')
                }
            },
            include: [{
                model: Ubicacion,
                include: [{ model: UbicacionEspecifica }]
            }]
        })
        .then(datos => {
            //console.log(datos.ubicacions[0].dataValues.ubicacion_especifica)
            if(datos!=null){
            respuesta.empresa.id_empresa = datos.id;
            respuesta.empresa.nombre_empresa = datos.nombre;
            respuesta.empresa.razon_social=datos.razon_social;
            respuesta.empresa.ubicacion_especifica = [];
            for (let i = 0; i < datos.ubicacions.length; i++) {
                respuesta.empresa.ubicacion_especifica.push({
                    tipo_vialidad: datos.ubicacions[i].dataValues.ubicacion_especifica.tipo_vialidad,
                    nombre_vialidad: datos.ubicacions[i].dataValues.ubicacion_especifica.nombre_vialidad,
                    numero_exterior: datos.ubicacions[i].dataValues.ubicacion_especifica.num_exterior,
                    letra_exterior: datos.ubicacions[i].dataValues.ubicacion_especifica.letra_exterior,
                    nombre_edificio: datos.ubicacions[i].ubicacion_especifica.dataValues.nombre_edifico,
                    piso: datos.ubicacions[i].dataValues.ubicacion_especifica.piso,
                    numero_interior: datos.ubicacions[i].dataValues.ubicacion_especifica.num_interior,
                    letra_interior: datos.ubicacions[i].dataValues.ubicacion_especifica.letra_interior,
                    manzana: datos.ubicacions[i].dataValues.ubicacion_especifica.manzana,
                    tipo_asentamiento: datos.ubicacions[i].dataValues.ubicacion_especifica.tipos_asentamiento,
                    nombre_asentamiento: datos.ubicacions[i].dataValues.ubicacion_especifica.nom_asentamiento,
                    tipo_centro_comercial: datos.ubicacions[i].dataValues.ubicacion_especifica.tipo_centro_com,
                    nombre_centro_comercial: datos.ubicacions[i].dataValues.ubicacion_especifica.nom_centro_com,
                    numero_local: datos.ubicacions[0].dataValues.ubicacion_especifica.nlocal,
                    latitud: datos.ubicacions[i].dataValues.ubicacion_especifica.latitud,
                    longitud: datos.ubicacions[i].dataValues.ubicacion_especifica.longitud
                });
            }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);
                    res.end(parser("empresa", respuesta.empresa, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;
            }
        } else {
            res.status(404);
            res.end();
            next();
            }
        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
    } else {
        res.status(404);
        res.end();
        next()
    }
});

router.get('/medios-masivos/:id/ubicacion-especifica/referencias', (req, res, next) => {
    let respuesta = { empresa: {} };
    if(Number.isInteger(Number(req.params.id))){
    sequealize.query(` select empresa.id,empresa.nombre,ubicacion.id_ubicacion,referencias.tipo_vialidad,referencias.nombre_vialidad from referencias 
    inner join  ubicacion_especifica on (referencias.id_especifica=ubicacion_especifica.id_especifica)
    inner join ubicacion on(ubicacion.id_ubicacion=ubicacion_especifica.id_ubicacion)
    inner join empresa on(empresa.id=ubicacion.id_empresa)
    where empresa.id=:id and empresa.idactividad like '51%'  
    `, {
            replacements: {
                id: req.params.id
            },
            type: sequealize.QueryTypes.SELECT
        })
        .then(datos => {
            if (datos.length > 0) {
                respuesta.empresa.id_empresa = datos[0].id;
                respuesta.empresa.nombre_empresa = datos[0].nombre;
                respuesta.empresa.ubicaciones_referencia ={ubicacion:[]};
                var ubicacion = [];
                for (let q = 0; q < datos.length; q++) {
                    if (!cb.existe_arr(ubicacion, datos[q].id_ubicacion)) {
                        ubicacion.push(datos[q].id_ubicacion)
                    }
                }
                for (let i = 0; i < ubicacion.length; i++) {
                    
                    let arre_ubicacion = [];
                    for (let j = 0; j < datos.length; j++) {
                        if (ubicacion[i] === datos[j].id_ubicacion) {
                            let obj_ubicacion = {};
                            obj_ubicacion.tipo_vialidad = datos[j].tipo_vialidad
                            obj_ubicacion.nombre_vialidad = datos[j].nombre_vialidad
                            respuesta.empresa.ubicaciones_referencia.ubicacion.push(obj_ubicacion);
                            //arre_ubicacion.push(obj_ubicacion);

                        }

                    }
                    //respuesta.empresa.ubicacion_referencia.ubicacion.push(arre_ubicacion);

                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresa", respuesta.empresa, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            }else {
                res.status(404);
                res.end();
                next();
            }
        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
    } else {
        res.status(404);
        res.end();
        next()
    }
});

router.get('/medios-masivos/actividades/:id', (req, res, next) => {
   let respuesta = { empresa: [] }
    let limites=cb.limites(req.query.limite_inferior,req.query.limite_superior);
    let orden=cb.orden(req.query.tipo_orden);
    console.log("li:"+limites[0]+"ls:"+limites[1]+"o:"+orden);
    if(limites[0]===-1||orden==="error"){
        res.status(404);
        res.end();
        next();
    }else{
    Empresa.findAll({
            order:[orden],
            include: [{
                model: Actividad,
                where: {
                    cve_scian: req.params.id
                }
            }]
        })
        .then(datos => {
            if (datos.length > 0) {
                if(limites[1]>datos.length){
                    limites[1]=datos.length;
                }
                for (let i = limites[0]; i < limites[1]; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social,
                        id_actividad: datos[i].dataValues.idactividad,
                        descripcion_actividad: datos[i].dataValues.actividad.desc_scian
                    })
                }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);
                    res.end(parser("empresas", respuesta, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;
            }
        } else {
                res.status(404);
                res.end();
                next();
            }
        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
    }
});

//RUTAS DE SEGUROS Y FINANCIERAS
router.get('/seguros-financieras', (req, res, next) => {
    let respuesta = { empresa: [] }
    let limites=cb.limites(req.query.limite_inferior,req.query.limite_superior);
    let orden=cb.orden(req.query.tipo_orden);
    let busqueda='%';
    if(req.query.nombre_empresa!=undefined){
        busqueda='%'+req.query.nombre_empresa+'%';
    }
    console.log("li:"+limites[0]+"ls:"+limites[1]+"o:"+orden);
    if(limites[0]===-1||orden==="error"){
        res.status(404);
        res.end();
        next();
    }else{
    Empresa.findAll({
            order:[orden],
            where: {
                idactividad: {
                    $ilike: (52 + '%')
                },
                nombre:{$ilike: busqueda}
            }
        })
        .then(datos => {

            if (datos.length > 0) {
                if(limites[1]>datos.length){
                    limites[1]=datos.length;
                }
                for (let i = limites[0]; i < limites[1]; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }

            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })
    }

});

router.get('/seguros-financieras/actividades', (req, res, next) => {
    let respuesta = { empresa: [] }
    let limites=cb.limites(req.query.limite_inferior,req.query.limite_superior);
    let orden=cb.orden(req.query.tipo_orden);
    let busqueda='%';
    if(req.query.nombre_actividad!=undefined){
        busqueda='%'+req.query.nombre_actividad+'%';
    }
    if(limites[0]===-1||orden==="error"){
        res.status(404);
        res.end();
        next();
    }else{
    Empresa.findAll({
            order:[orden],
            where: {
                idactividad: {
                    $ilike: (52 + '%')
                }
            },
            include: [{ model: Actividad,where: {desc_scian :{$ilike: busqueda}}}]
        })
        .then(datos => {
            if (datos.length > 0) {
                if(limites[1]>datos.length){
                    limites[1]=datos.length;
                }
                for (let i = limites[0]; i < limites[1]; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social,
                        id_actividad: datos[i].dataValues.idactividad,
                        descripcion_actividad: datos[i].dataValues.actividad.desc_scian
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next();
            }
        })
        .catch(data => {})
    }
});

router.get('/seguros-financieras/:id', (req, res, next) => {
    let respuesta = { empresa: [] }
    Empresa.findAll({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (52 + '%')
                }
            },
            include: [{ model: Actividad }]
        })
        .then(datos => {
            if (datos.length > 0) {
                for (let i = 0; i < datos.length; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social,
                        personal_ocupado: datos[i].dataValues.personal_ocupado,
                        clave_actividad: datos[i].dataValues.idactividad,
                        descripcion_actividad: datos[i].dataValues.actividad.desc_scian
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })
});

router.get('/seguros-financieras/:id/contactos', (req, res, next) => {
    let respuesta = { empresa: {} };
    if(Number.isInteger(Number(req.params.id))){
    Empresa.findAll({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (52 + '%')
                }
            },
            include: [{ model: Correos }, { model: Telefonos }],
        })
        .then(datos => {
            //console.log(datos[0].dataValues.correos)
            if (datos.length > 0) {
                respuesta.empresa.id_empresa = datos[0].dataValues.id
                respuesta.empresa.nombre = datos[0].dataValues.nombre
                respuesta.empresa.razon_social=datos[0].razon_social
                respuesta.empresa.sitio_web = datos[0].dataValues.sitio_web;
                respuesta.empresa.correos = [];
                respuesta.empresa.telefonos = [];
                for (let i = 0; i < datos[0].dataValues.correos.length; i++) {
                    respuesta.empresa.correos.push(datos[0].dataValues.correos[i].dataValues.correo)
                }
                for (let i = 0; i < datos[0].dataValues.telefonos.length; i++) {
                    respuesta.empresa.telefonos.push(datos[0].dataValues.telefonos[i].dataValues.telefono)
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("empresa", respuesta.empresa, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
    } else {
        res.status(404);
        res.end();
        next()
    }
});

router.get('/seguros-financieras/:id/ubicacion-general', (req, res, next) => {
    let respuesta = { empresa: {} };
    if(Number.isInteger(Number(req.params.id))){
    Empresa.findOne({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (52 + '%')
                }
            },
            include: [{
                model: Ubicacion,

                include: [{
                    model: Entidad
                }]
            }]
        })
        .then(datos => {
            if(datos!=null){
            respuesta.empresa.id_empresa = datos.id;
            respuesta.empresa.nombre_empresa = datos.nombre
            respuesta.empresa.razon_social=datos.razon_social
            respuesta.empresa.ubicacion_general = []
                //console.log(datos.ubicacions[0].entidad.dataValues.nombre);
            for (let i = 0; i < datos.ubicacions.length; i++) {
                respuesta.empresa.ubicacion_general.push({
                    clave_entidad: datos.ubicacions[i].dataValues.clave_entidad,
                    nombre_entidad: datos.ubicacions[i].entidad.dataValues.nombre,
                    clave_municipio: datos.ubicacions[i].dataValues.clave_municipio,
                    nombre_municipio: datos.ubicacions[i].dataValues.nombre_municipio,
                    clave_localidad: datos.ubicacions[i].dataValues.clave_localidad,
                    nombre_localidad: datos.ubicacions[i].dataValues.nombre_localidad,
                    codigo_postal: datos.ubicacions[i].dataValues.cp,
                    area_geografica_basica: datos.ubicacions[i].dataValues.agbasica
                })
            }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);
                    res.end(parser("empresa", respuesta.empresa, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;
            }
        } else {
                res.status(404);
                res.end();
                next();
            }
        })
        .catch(err => {
        })
    } else {
        res.status(404);
        res.end();
        next()
    }
});

router.get('/seguros-financieras/:id/ubicacion-especifica', (req, res, next) => {
    let respuesta = { empresa: {} }
    if(Number.isInteger(Number(req.params.id))){
    Empresa.findOne({
            where: {
                id: req.params.id,
                idactividad: {
                    $ilike: (52 + '%')
                }
            },
            include: [{
                model: Ubicacion,
                include: [{ model: UbicacionEspecifica }]
            }]
        })
        .then(datos => {
            if(datos!=null){
            //console.log(datos.ubicacions[0].dataValues.ubicacion_especifica)
            respuesta.empresa.id_empresa = datos.id;
            respuesta.empresa.nombre_empresa = datos.nombre;
            respuesta.empresa.razon_social=datos.razon_social
            respuesta.empresa.ubicaciones_especificas = [];
            for (let i = 0; i < datos.ubicacions.length; i++) {
                respuesta.empresa.ubicaciones_especificas.push({
                    tipo_vialidad: datos.ubicacions[i].dataValues.ubicacion_especifica.tipo_vialidad,
                    nombre_vialidad: datos.ubicacions[i].dataValues.ubicacion_especifica.nombre_vialidad,
                    numero_exterior: datos.ubicacions[i].dataValues.ubicacion_especifica.num_exterior,
                    letra_exterior: datos.ubicacions[i].dataValues.ubicacion_especifica.letra_exterior,
                    nombre_edificio: datos.ubicacions[i].ubicacion_especifica.dataValues.nombre_edifico,
                    piso: datos.ubicacions[i].dataValues.ubicacion_especifica.piso,
                    numero_interior: datos.ubicacions[i].dataValues.ubicacion_especifica.num_interior,
                    letra_interior: datos.ubicacions[i].dataValues.ubicacion_especifica.letra_interior,
                    manzana: datos.ubicacions[i].dataValues.ubicacion_especifica.manzana,
                    tipo_asentamiento: datos.ubicacions[i].dataValues.ubicacion_especifica.tipos_asentamiento,
                    nombre_asentamiento: datos.ubicacions[i].dataValues.ubicacion_especifica.nom_asentamiento,
                    tipo_centro_comercial: datos.ubicacions[i].dataValues.ubicacion_especifica.tipo_centro_com,
                    nombre_centro_comercial: datos.ubicacions[i].dataValues.ubicacion_especifica.nom_centro_com,
                    numero_local: datos.ubicacions[0].dataValues.ubicacion_especifica.nlocal,
                    latitud: datos.ubicacions[i].dataValues.ubicacion_especifica.latitud,
                    longitud: datos.ubicacions[i].dataValues.ubicacion_especifica.longitud
                });
            }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);
                    res.end(parser("empresa", respuesta.empresa, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;
            }
        } else {
                res.status(404);
                res.end();
                next();
            }
        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
    } else {
        res.status(404);
        res.end();
        next()
    }
});

router.get('/seguros-financieras/:id/ubicacion-especifica/referencias', (req, res, next) => {
    let respuesta = { empresa: {} };
    if(Number.isInteger(Number(req.params.id))){
    sequealize.query(` select empresa.id,empresa.nombre,ubicacion.id_ubicacion,referencias.tipo_vialidad,referencias.nombre_vialidad from referencias 
    inner join  ubicacion_especifica on (referencias.id_especifica=ubicacion_especifica.id_especifica)
    inner join ubicacion on(ubicacion.id_ubicacion=ubicacion_especifica.id_ubicacion)
    inner join empresa on(empresa.id=ubicacion.id_empresa)
    where empresa.id=:id and empresa.idactividad like '52%' 
    `, {
            replacements: {
                id: req.params.id
            },
            type: sequealize.QueryTypes.SELECT
        })
        .then(datos => {
            if (datos.length > 0) {
                respuesta.empresa.id_empresa = datos[0].id;
                respuesta.empresa.nombre_empresa = datos[0].nombre;
                respuesta.empresa.razon_social=datos[0].razon_social
                respuesta.empresa.ubicaciones_referencia = {ubicacion:[]};
                var ubicacion = [];
                for (let q = 0; q < datos.length; q++) {
                    if (!cb.existe_arr(ubicacion, datos[q].id_ubicacion)) {
                        ubicacion.push(datos[q].id_ubicacion)
                    }

                }

                for (let i = 0; i < ubicacion.length; i++) {
                    
                    let arre_ubicacion = [];
                    for (let j = 0; j < datos.length; j++) {
                        if (ubicacion[i] === datos[j].id_ubicacion) {
                            let obj_ubicacion = {};
                            obj_ubicacion.tipo_vialidad = datos[j].tipo_vialidad
                            obj_ubicacion.nombre_vialidad = datos[j].nombre_vialidad
                            respuesta.empresa.ubicaciones_referencia.ubicacion.push(obj_ubicacion)
                            //arre_ubicacion.push(obj_ubicacion);

                        }

                    }
                    //respuesta.ubicacion_referencia.push(arre_ubicacion);

                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);

                        res.end(parser("empresa", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;

                }



            }else {
                res.status(404);
                res.end();
                next();
            }



        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
    } else {
        res.status(404);
        res.end();
        next()
    }
});

router.get('/seguros-financieras/actividades/:id', (req, res, next) => {
   let respuesta = { empresa: [] }
    let limites=cb.limites(req.query.limite_inferior,req.query.limite_superior);
    let orden=cb.orden(req.query.tipo_orden);
    console.log("li:"+limites[0]+"ls:"+limites[1]+"o:"+orden);
    if(limites[0]===-1||orden==="error"){
        res.status(404);
        res.end();
        next();
    }else{
    Empresa.findAll({
            order:[orden],
            include: [{
                model: Actividad,
                where: {
                    cve_scian: req.params.id
                }
            }]
        })
        .then(datos => {
            if (datos.length > 0) {
                if(limites[1]>datos.length){
                    limites[1]=datos.length;
                }
                for (let i = limites[0]; i < limites[1]; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social,
                        id_actividad: datos[i].dataValues.idactividad,
                        descripcion_actividad: datos[i].dataValues.actividad.desc_scian
                    })
                }
            switch (cb.cabeceras(req.headers.accept)) {
                case 'application/xml':
                    res.setHeader('Content-Type', 'application/xml');
                    res.status(200);
                    res.end(parser("empresas", respuesta, { useCDATA: false }));
                    break;
                case 'application/json':
                    res.setHeader('Content-Type', 'application/json');
                    res.json(200, respuesta);
                    break;
                default:
                    res.status(406);
                    res.end();
                    break;
            }
        } else {
                res.status(404);
                res.end();
                next();
            }
        })
        .catch(err => {
            console.log(`${err.stack}`)
        })
    }
});

//RUTAS ESTADOS
router.get('/estados', (req, res, next) => {
    let respuesta = { empresa: [] }
    let limites=cb.limites(req.query.limite_inferior,req.query.limite_superior);
    let orden=cb.orden(req.query.tipo_orden);
    let busqueda='%';
    if(req.query.nombre_empresa!=undefined){
        busqueda='%'+req.query.nombre_empresa+'%';
    }

    if(limites[0]===-1||orden==="error"){
        res.status(404);
        res.end();
        next();
    }else{
    Empresa.findAll({
            order:[orden],
            where: {nombre:{$ilike: busqueda}},
            include: [{
                model: Ubicacion,
                include: [{ model: Entidad }]
            }]
        })
        .then(datos => {
            if (datos.length > 0) {
                if(limites[1]>datos.length){
                    limites[1]=datos.length;
                }
                for (let i = limites[0]; i < limites[1]; i++) {
                    respuesta.empresa.push({
                        id_empresa: datos[i].dataValues.id,
                        nombre_empresa: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social,
                        clave_estado: datos[i].dataValues.ubicacions[0].dataValues.clave_entidad,
                        nombre: datos[i].dataValues.ubicacions[0].dataValues.entidad.dataValues.nombre
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })
    }
});

router.get('/estados/:id', (req, res, next) => {
    let respuesta = { empresa: [] }
    let limites=cb.limites(req.query.limite_inferior,req.query.limite_superior);
    let orden=cb.orden(req.query.tipo_orden);
    let busqueda='%';
    if(req.query.nombre_empresa!=undefined){
        busqueda='%'+req.query.nombre_empresa+'%';
    }
    if(limites[0]===-1||orden==="error"){
        res.status(404);
        res.end();
        next();
    }else{
    Empresa.findAll({
            order:[orden],
            where: {nombre:{$ilike: busqueda}},
            include: [{
                model: Ubicacion,
                where: {
                    clave_entidad: req.params.id
                },
                include: [{
                    model: Entidad
                }]
            }]
        })
        .then(datos => {
            if (datos.length > 0) {
                if(limites[1]>datos.length){
                    limites[1]=datos.length;
                }
                for (let i = limites[0]; i < limites[1]; i++) {
                    respuesta.empresa.push({
                        id_empresa: datos[i].dataValues.id,
                        nombre_empresa: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social,
                        clave_estado: datos[i].dataValues.ubicacions[0].dataValues.clave_entidad,
                        nombre: datos[i].dataValues.ubicacions[0].dataValues.entidad.dataValues.nombre
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })
    }

});

router.get('/:id', (req, res, next) => {
   let respuesta = { empresa: [] }
    Empresa.findAll({
            where: {
                id: req.params.id,
            },
            include: [{ model: Actividad }]
        })
        .then(datos => {
            if (datos.length > 0) {
                for (let i = 0; i < datos.length; i++) {
                    respuesta.empresa.push({
                        id: datos[i].dataValues.id,
                        nombre: datos[i].dataValues.nombre,
                        razon_social: datos[i].dataValues.razon_social,
                        personal_ocupado: datos[i].dataValues.personal_ocupado,
                        clave_actividad: datos[i].dataValues.idactividad,
                        descripcion_actividad: datos[i].dataValues.actividad.desc_scian
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("empresas", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next()
            }
        })
        .catch(err => {
            res.status(404);
            res.end();
            next();
        })
});












/*********************************POST*************************** */

router.post('/', (req, res, next) => {
    var datos_peticion = req.body;
    sequealize.query('select max(id) from empresa', {
            type: sequealize.QueryTypes.SELECT
        })
        .then(data => {
            console.log(datos_peticion);
            var id_nuevo = parseInt(data[0].max) + 1;
            return sequealize.transaction(trans => {
                    return Empresa.create({
                        id: id_nuevo,
                        nombre: datos_peticion.nombre_empresa,
                        razon_social: datos_peticion.razon_social,
                        personal_ocupado: datos_peticion.personal_ocupado,
                        sitio_web: datos_peticion.sitio_web,
                        tipo_establecimiento: datos_peticion.tipo_establecimiento,
                        fecha_incorporacion: datos_peticion.fecha_incorporacion,
                        idactividad: datos_peticion.id_actividad
                    }, {
                        transaction: trans
                    })
                })
                .then(data => {
                    sequealize.query('select max(id_ubicacion) from ubicacion', {
                            type: sequealize.QueryTypes.SELECT
                        })
                        .then(data => {
                            var id_ubicacion_nuevo = parseInt(data[0].max) + 1;
                            console.log(id_nuevo);
                            return sequealize.transaction(trans => {
                                    return Ubicacion.create({
                                        id_ubicacion: id_ubicacion_nuevo,
                                        id_empresa: id_nuevo,
                                        clave_entidad: '01',
                                        clave_municipio: 'N/D',
                                        nombre_municipio: 'N/D',
                                        clave_localidad: 'N/D',
                                        nombre_localidad: 'N/D',
                                        cp: 'N/D',
                                        agbasica: 'N/D'

                                    }, {
                                        transaction: trans
                                    })
                                })
                                .then(data => {
                                    return sequealize.transaction(trans => {
                                            return UbicacionEspecifica.create({
                                                id_ubicacion: id_ubicacion_nuevo,
                                                id_especifica: id_ubicacion_nuevo,
                                                tipo_vialidad: 'N/D',
                                                nombre_vialidad: 'N/D',
                                                num_exterior: 'N/D',
                                                letra_exterior: 'N/D',
                                                nombre_edificio: 'N/D',
                                                piso: 'N/D',
                                                num_interior: 0,
                                                letra_interior: 'N/D',
                                                manzana: 'N/D',
                                                tipos_asentamiento: 'N/D',
                                                nom_asentamiento: 'N/D',
                                                tipo_centro_com: 'N/D',
                                                nom_centro_com: 'N/D',
                                                nlocal: 'N/D',
                                                latitud: 0.00,
                                                longitud: 0.00
                                            }, {
                                                transaction: trans
                                            })
                                        })
                                        .then(data => {
                                            return sequealize.transaction(trans => {
                                                    return Referencias.create({
                                                        id_especifica: id_ubicacion_nuevo,
                                                        tipo_vialidad: 'N/D',
                                                        nombre_vialidad: '01'
                                                    }, {
                                                        transaction: trans
                                                    })
                                                })
                                                .then(data => {
                                                    return sequealize.transaction(trans => {
                                                            return Referencias.create({
                                                                id_especifica: id_ubicacion_nuevo,
                                                                tipo_vialidad: 'N/D',
                                                                nombre_vialidad: '02'
                                                            }, {
                                                                transaction: trans
                                                            })
                                                        })
                                                        .then(data => {
                                                            return sequealize.transaction(trans => {
                                                                    return Referencias.create({
                                                                        id_especifica: id_ubicacion_nuevo,
                                                                        tipo_vialidad: 'N/D',
                                                                        nombre_vialidad: '03'
                                                                    }, {
                                                                        transaction: trans
                                                                    })
                                                                })
                                                                .then(data => {
                                                                    res.status(201);
                                                                    res.end();
                                                                    next();
                                                                })
                                                        })
                                                })
                                        })
                                })
                        })
                })
        })
        .catch(err => {
            console.log(err.stack)
            res.status(400);
            res.end();
            next();
        })
})

router.put('/:id', (req, res, next) => {
    return sequealize.transaction(trans => {
            return Empresa.update({
                nombre: req.body.nombre_empresa,
                razon_social: req.body.razon_social,
                personal_ocupado: req.body.personal_ocupado,
                sitio_web: req.body.sitio_web,
                tipo_establecimiento: req.body.tipo_establecimiento,
                fecha_incorporacion: req.body.fecha_incorporacion,
                idactividad: req.body.id_actividad
            }, { where: { id: req.params.id } }, {
                returning: true,
                plain: true,
                transaction: trans
            })
        })
        .then(resultado => {
            res.status(201);
            res.end();
            next();
        })
        .catch(err => {
            res.status(400);
            res.end();
            next();
        });
})

router.post('/:id/telefono', (req, res, next) => {
    if (req.body.telefono != undefined) {
        return sequealize.transaction(trans => {
                return Telefonos.create({
                    id_empresa: req.params.id,
                    telefono: req.body.telefono
                })
            })
            .then(data => {
                res.status(201);
                res.end();
                next();
            })
            .catch(err => {
                console.log(err.stack)
                res.status(400);
                res.end();
                next();
            })
    } else {
        res.status(400);
        res.end();
        next();
    }
})


router.put('/:id/telefono', (req, res, next) => {
    if (req.body.telefono_nuevo != undefined && req.body.telefono_actual != undefined) {
        return sequealize.transaction(trans => {
                return Telefonos.update({
                    telefono: req.body.telefono_nuevo
                }, {
                    where: {
                        id_empresa: req.params.id,
                        telefono: req.body.telefono_actual
                    }
                }, {
                    returning: true,
                    plain: true,
                    transaction: trans
                })
            })
            .then(data => {
                res.status(201);
                res.end();
                next();
            })
            .catch(err => {
                console.log(err.stack)
                res.status(400);
                res.end();
                next();
            })
    } else {
        res.status(400);
        res.end();
        next();
    }
})

router.post('/:id/correo', (req, res, next) => {
    if (req.body.correo != undefined) {
        return sequealize.transaction(trans => {
                return Correos.create({
                    id_empresa: req.params.id,
                    correo: req.body.correo
                })
            })
            .then(data => {
                res.status(201);
                res.end();
                next();
            })
            .catch(err => {
                console.log(err.stack)
                res.status(400);
                res.end();
                next();
            })
    } else {
        res.status(400);
        res.end();
        next();
    }
})


router.put('/:id/correo', (req, res, next) => {
    if (req.body.correo_nuevo != undefined && req.body.correo_actual != undefined) {
        return sequealize.transaction(trans => {
                return Correos.update({
                    correo: req.body.correo_nuevo
                }, {
                    where: {
                        id_empresa: req.params.id,
                        correo: req.body.correo_actual
                    }
                }, {
                    returning: true,
                    plain: true,
                    transaction: trans
                })
            })
            .then(data => {
                res.status(201);
                res.end();
                next();
            })
            .catch(err => {
                console.log(err.stack)
                res.status(400);
                res.end();
                next();
            })
    } else {
        res.status(400);
        res.end();
        next();
    }
})

router.put('/:id/ubicacion-general', (req, res, next) => {
    var datos_peticion = JSON.parse(req.body)
    console.log(datos_peticion.datos_actualizar);

    return sequealize.transaction(trans => {
            return Ubicacion.update(datos_peticion.datos_actualizar, {
                where: {
                    id_empresa: req.params.id,
                }
            }, {
                returning: true,
                plain: true,
                transaction: trans
            })
        })
        .then(data => {
            console.log(data[0])
            if (data[0] !== 0) {
                res.status(201);
                res.end();
                next();

            } else {

                res.status(400);
                res.end();
            }

        })
        .catch(err => {
            console.log(err.stack)
            res.status(400);
            res.end();
            next();
        })




})

router.put('/:id/ubicacion-especifica', (req, res, next) => {
    var datos_peticion = JSON.parse(req.body)

    sequealize.query("select ubicacion_especifica.id_ubicacion from ubicacion_especifica inner join ubicacion on (ubicacion.id_ubicacion= ubicacion_especifica.id_ubicacion) inner join empresa on(id=id_empresa)where id=:id", {
            replacements: {
                id: req.params.id
            },
            type: sequealize.QueryTypes.SELECT
        })
        .then(data => {
            console.log(data[0].id_ubicacion)
            var id_ubicacion_busca = data[0].id_ubicacion;
            return sequealize.transaction(trans => {
                    return UbicacionEspecifica.update(datos_peticion.datos_actualizar, {
                        where: {
                            id_ubicacion: id_ubicacion_busca
                        }
                    }, {
                        returning: true,
                        plain: true,
                        transaction: trans
                    })
                })
                .then(data => {
                    if (data[0] !== 0) {
                        res.status(201);
                        res.end();
                        next();

                    } else {

                        res.status(400);
                        res.end();
                    }
                })


        })
        .catch(err => {
            console.log(err.stack)
            res.status(400);
            res.end();
            next();
        })


})

router.put('/:id/referencias', (req, res, next) => {
    var datos_peticion = JSON.parse(req.body)
    sequealize.query("select ubicacion_especifica.id_ubicacion from ubicacion_especifica inner join ubicacion on (ubicacion.id_ubicacion= ubicacion_especifica.id_ubicacion) inner join empresa on(id=id_empresa)where id=:id", {
            replacements: {
                id: req.params.id,

            },
            type: sequealize.QueryTypes.SELECT
        })
        .then(data => {
            console.log(datos_peticion.nombre_vialidad)
            var id_ubicacion_busca = data[0].id_ubicacion;
            return sequealize.transaction(trans => {
                    return Referencias.update(datos_peticion.datos_actualizar, {
                        where: {
                            id_especifica: id_ubicacion_busca,
                            nombre_vialidad: datos_peticion.nombre_vialidad
                        }
                    }, {
                        returning: true,
                        plain: true,
                        transaction: trans
                    })
                })
                .then(data => {
                    if (data[0] !== 0) {
                        res.status(201);
                        res.end();
                        next();

                    } else {

                        res.status(400);
                        res.end();
                    }
                })


        })
        .catch(err => {
            console.log(err.stack)
            res.status(400);
            res.end();
            next();
        })




});

router.del('/:id', (req, res, next) => {
    sequealize.query("select ubicacion_especifica.id_ubicacion from ubicacion_especifica inner join ubicacion on (ubicacion.id_ubicacion= ubicacion_especifica.id_ubicacion) inner join empresa on(id=id_empresa)where id=:id", {
            replacements: {
                id: req.params.id
            },
            type: sequealize.QueryTypes.SELECT
        })
        .then(data => {
            console.log(data[0]);
            if (data[0] !== undefined) {
                var id_ubicacion_busca = data[0].id_ubicacion;
                return sequealize.transaction(trans => {
                        return Referencias.destroy({
                            where: {
                                id_especifica: id_ubicacion_busca
                            }
                        }, { transaction: trans })
                    })
                    .then(data => {
                        return sequealize.transaction(trans => {
                                return Correos.destroy({
                                    where: {
                                        id_empresa: req.params.id
                                    }
                                }, { transaction: trans })
                            })
                            .then(data => {
                                return sequealize.transaction(trans => {
                                        return Telefonos.destroy({
                                            where: {
                                                id_empresa: req.params.id
                                            }
                                        }, { transaction: trans })
                                    })
                                    .then(data => {
                                        return sequealize.transaction(trans => {
                                                return UbicacionEspecifica.destroy({
                                                    where: {
                                                        id_ubicacion: id_ubicacion_busca
                                                    }
                                                }, { transaction: trans })
                                            })
                                            .then(data => {
                                                return sequealize.transaction(trans => {
                                                    return Ubicacion.destroy({
                                                        where: {
                                                            id_empresa: req.params.id
                                                        }
                                                    }, { transaction: trans })
                                                })
                                                .then(data => {
                                                    return sequealize.transaction(trans => {
                                                            return Empresa.destroy({
                                                                where: {
                                                                    id: req.params.id
                                                                }
                                                            }, { transaction: trans })
                                                        })
                                                        .then(data => {
                                                            res.status(204);
                                                            res.end();
                                                            next();
                                                        })
                                                })
                                            })
                                    })
                            })
                    })
            } else {

                return sequealize.transaction(trans => {
                        return Referencias.destroy({
                            where: {
                                id_especifica: id_ubicacion_busca
                            }
                        }, { transaction: trans })

                    })
                    .then(data => {
                        return sequealize.transaction(trans => {
                                return Correos.destroy({
                                    where: {
                                        id_empresa: req.params.id
                                    }
                                }, { transaction: trans })

                            })
                            .then(data => {
                                return sequealize.transaction(trans => {
                                        return Telefonos.destroy({
                                            where: {
                                                id_empresa: req.params.id
                                            }
                                        }, { transaction: trans })

                                    })
                                    .then(data => {
                                        return sequealize.transaction(trans => {
                                                return Ubicacion.destroy({
                                                    where: {
                                                        id_empresa: req.params.id
                                                    }
                                                }, { transaction: trans })

                                            })
                                            .then(data => {
                                                return sequealize.transaction(trans => {
                                                        return Empresa.destroy({
                                                            where: {
                                                                id: req.params.id
                                                            }
                                                        }, { transaction: trans })

                                                    })
                                                    .then(data => {
                                                        res.status(204);
                                                        res.end();
                                                        next();
                                                    })
                                            })
                                    })
                            })
                    })
            }
        })
        .catch(err => {
            console.log(err.stack)
            res.status(400);
            res.end();
            next();
        })
})




/*
		


*/

module.exports = router;